#include "numberconversion.h"

using namespace std;

int romantoint(std::string roman_numeral)
{
  ////map vector function from std maps here a charcter to corrsponding int
  
  map <char, int> m = {{'I', 1}, {'V', 5},{'X', 10},{'L', 50},
        {'C', 100},{'D', 500},{'M', 1000}};
  
  int intnumber = 0;
	
  ////long unsigned integer is used for coisistency because sting.length function resturns long unsigned integer

	
  for(long unsigned int i = 0; i <roman_numeral.length(); i++)
  {
   //if the ith  element of m map vector matches the prior one or is less than it then add the string to number value to the intnumber
   //else subtract this number from previous

   if(m[roman_numeral[i+1]] <= m[roman_numeral[i]])
    intnumber += m[roman_numeral[i]]; 
   else intnumber -= m[roman_numeral[i]];

   // assume the input is XIX; thus roman_numeral[0+1] is I ->(1) which is < roman_numeral[0] is X->(10) i.e. intnumber=0+10,
   // next loop: roman_numeral [1+1] is X ->10 which is > roman_numeral[1]is I-> (1) i.e. intnumber=-1+10=9;
   //last loop:roman_numeral [2+1]is undefined ->(0)which is <roman_numera[2] is X->10 i.e. intnumber=9+10=19
  }
  return intnumber;
}

std::string inttoroman(int number)
{
  int num = number;
  
  int i = {"","I","II","III","IV","V","VI","VII","VIII","IX"};
  int x = {"","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"};
  int c = {"","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"};
  int m = {"","M","MM","MMM"};

  string thousands = m[num/1000];
  string hundereds = c[(num%1000)/100];
  string tens =  x[(num%100)/10];
  string ones = i[num%10];
         
  string ans = thousands + hundereds + tens + ones;
         
  return ans; 
}
